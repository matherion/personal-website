---
title: "Alcohol of drugs: wat is slechter voor je?"
author: Gjalt-Jorn Peters
date: '2023-07-22'
draft: false
categories: ["mastodon"]
tags: []
editor_options: 
  chunk_output_type: console
---

```{r, echo=FALSE}

mastodonId <- "110782064245732455";

```

Dit is een Mastodon draadje. [Het originele draadje is hier beschikbaar](https://mastodon.nl/@matherion/`r mastodonId`){.external target="_blank"}:

<iframe src="https://mastodon.nl/@matherion/`r mastodonId`/embed" class="mastodon-embed" style="max-width: 100%; border: 0" width="400" allowfullscreen="allowfullscreen"></iframe><script src="https://mastodon.nl/embed.js" async="async"></script>

-----

De Universiteit van Nederland ([https://universiteitvannederland.nl](https://universiteitvannederland.nl){.external target="_blank"}) is een fantastische organisatie die zich hard inzet om wetenschap toegankelijk te maken voor een breed publiek.

Wetenschappers zijn daar niet altijd goed in; maar de mensen van de Universiteit van Nederland kunnen heel goed met wetenschappers samenwerken om dingen uiteindelijk toch duidelijk uit te leggen voor iedereen.

En nog gratis ook! 🤩

-----

Ik heb het geluk dat ze met mij wilden samenwerken over een filmpje om de uitgebreide zogenaamde "multi decision multi criteria decision analysis" over MDMA beleid te produceren ([https://www.universiteitvannederland.nl/college/moeten-we-drugs-legaliseren](https://www.universiteitvannederland.nl/college/moeten-we-drugs-legaliseren){.external target="_blank"}).

Onlangs maakten we samen weer een nieuw filmpje, en in deze Mastodon thread en blog post ([https://sciencer.eu/posts/2023-07-alcohol-of-drugs-wat-is-slechter.html](https://gjp.one/posts/2023-07-alcohol-of-drugs-wat-is-slechter.html){}) geef ik een inkijkje in het proces en wat meer achtergrond informatie.

Eerst het proces en dan de info 🙂

-----

Het start met een intake waarin iemand van de Universiteit van Nederland het onderwerp met je doorneemt, en jullie samen de grove lijnen van een script bedenken. Hierna volgen nog een of meer sessies om dit uit te werken.

Hierbij let de Universiteit van Nederland goed op dat er een balans wordt getroffen tussen correctheid en begrijpelijkheid. Als wetenschapper leer je hier onder andere van dat je vooral een hoop niet kunt vertellen 🙂

-----

In ons geval wilden we iets doen met de vergelijking van schadelijkheid van drugs. David Nutt heeft in 2010 in de Lancet een bekende ranking van drugs gepubliceerd met de Multi-Criteria Decision Analysis methode [zie https://doi.org/djcthv](https://doi.org/djcthv){.external target="_blank"}.

We wilden deze uitleggen, maar daarvoor moesten we een eenvoudigere utivoering hebben, met minder drugs en maar een paar uitkomsten ('criteria').

![](../img/nutt---drug-ranking.png){fig-alt="The drug ranking of Nutt (2010), with alcohol (72 points), heroin (55 points), crack cocaine (54 points), methamphetamine (33 points), and cocaine (27 points) in the lead, followed by cannabis (20 points) in the 8th place, and ecstasy (9 points) in the 17th place."}

-----

We besloten daarom om een versimpelde versie van die oefening uit te voeren om het concept uit te kunnen leggen.

Hiervoor kozen we 5 drugs (alcohol, cocaine, cannabis, ecstasy, en koffie) en 4 uitkomsten of criteria (schade voor de gebruiker zelf, schade voor anderen, verslavingsgevoeligheid, en voordelen van gebruik).

Ik maakte hier een "Nano-MCDA" van die experts hebben ingevuld (preventiewerkers, artsen, wetenschappers, etc).

Je kunt de data bekijken op [https://matherion.gitlab.io/nano-mcda-uvnl](https://matherion.gitlab.io/nano-mcda-uvnl){.external target="_blank"}.

-----

Bij deze specifieke rubriek, de Werkplaats, werk je bovendien met 'props', als in, je kunt ook echt met objecten werken. Da's superleuk, maar dan moet je natuurlijk wel bedenken hoe die zinnig in te zetten zijn om het verhaal beter uit te leggen.

In ons geval wilde we de staafdiagram laten zien, dus daar gebruikten we blokjes voor.

We wilden bovendien uitleggen waarom ecstasy niet (psychofarmacologisch) verslavend is, en dat hebben we uiteindelijk met knikkers gedaan.

-----

De opnames waren in Amsterdam. Ik was daar om half elf (ik kwam uit Maastricht tenslotte), en we zijn ongeveer bezig geweest vanaf 11:00, en dan dik vier uur tot iets over 15:00.

De Universiteit van Nederland was met z'n vieren, dus in totaal waren we met z'n vijven - dat is al 20 "mens-uur" dus, en dan tellen we de voorbereiding en het editen niet eens mee... 😶

![](../img/uvnl---drugs---timelapse.gif){height="25%" width="25%" fig-alt="Een timelapse video van de opnames: ik sta overwegend achter de werkbank, de cameramensen achter de camera's, en de rest komt zo nu en dan in beeld en verdwijnt dan weer."}

-----

Het resultaat, "Alcohol of drugs: wat is slechter voor je?" staat op [https://youtu.be/dlCGnS4AyOg](https://youtu.be/dlCGnS4AyOg){.external target="_blank"}.

Als je dit interessant vindt en meer informatie wil, staat op de site van de Jellinek de samenvatting van een uitgebreide studie die in Nederland is gedaan:  [https://www.jellinek.nl/vraag-antwoord/welke-drug-is-de-gevaarlijkste](https://www.jellinek.nl/vraag-antwoord/welke-drug-is-de-gevaarlijkste){.external target="_blank"}.

Ik zal nu nog wat extra uitleg geven over vragen die mensen in de comments op YouTube en Nu.nl stelden.

-----

Een veel gestelde vraag was of er wel rekening werd gehouden met de standaardinname van de verschillende drugs. 

Het is absoluut waar dat dosis bepaalt hoe schadelijk iets is. Als je bijvoorbeeld drie liter water drinkt in een uur, kun je daaraan overlijden. Ook voor alcohol en ecstasy geldt dat als je er te veel van gebruikt, je daar diezelfde dag nog aan kunt overlijden.

-----

Het is ook zo dat een biertje (ongeveer 10 gram alcohol) veel minder schadelijk is dan twintig biertjes (ongeveer 200 gram alcohol; zie ook https://en.wikipedia.org/wiki/Standard_drink).

En een kwart pil (zeg 45 milligram MDMA) is minder schadelijk dan een hele pil (zeg 180 milligram MDMA).

Tegelijkertijd is er geen standaardinname van middelen.

-----

Er zijn wel suggesties: de WHO adviseert bijvoorbeeld om geen alcohol te drinken, en als je toch drinkt, maximaal 1 eenheid (dus 1 pilsje, of tweederde speciaalbiertje, of een glas wijn, etc).

En voor ecstasy is er een zogenaamd 'harm reduction' advies om maximaal 1-1.5 milligram MDMA per kilo lichaamsgewicht te gebruiken (dus als je 70 kilo weegt, maximaal ongeveer 105 gram MDMA; zie ook [https://www.jellinek.nl/informatie-over-alcohol-drugs/drugs/xtc-mdma/risicos/](https://www.jellinek.nl/informatie-over-alcohol-drugs/drugs/xtc-mdma/risicos/){.external target="_blank"}).

-----

En dan zijn er nog gemiddelden: je kunt uitrekenen hoeveel alcohol Nederlanders gemiddeld drinken per week of per dag.

Maar op een zaterdag ligt dat gemiddelde een stuk hoger dan op een dinsdag; en de meeste mensen drinken (veel) minder of (veel) meer dan dat gemiddelde: bijna niemand zit precies op het gemiddelde.

Dat geldt altijd voor gemiddelden; er is een gemiddelde leeftijd in Nederland (42 jaar), maar de meeste mensen zijn (veel) jonger of (veel) ouder ([https://www.cbs.nl/nl-nl/visualisaties/dashboard-bevolking/leeftijd/bevolking](https://www.cbs.nl/nl-nl/visualisaties/dashboard-bevolking/leeftijd/bevolking){.external target="_blank"}).

-----

Als we naar effecten van drugs kijken, is er geen 'typische gebruiker' voorhanden waar we naar kijken, want er bestaat geen typische gebruiker.

Meestal kijk je daarom naar de effecten zoals je ze nu ziet in de maatschappij, gecombineerd met wat je weet over de middelen.

-----

Als ecstasy bijvoorbeeld legaal zou zijn, is de verwachting dat gebruik iets toe zou nemen, maar niet veel (daar zijn ook weer allerlei redenen voor).

Dit soort overwegingen kun je ook meenemen, om zo te corrigeren voor de verschillende legale status van de drugs.

-----

Een andere vraag die mensen hadden, is of er werd meegenomen in de inschattingen hoeveel de verschillende drugs worden gebruikt.

Dat wordt inderdaad meegenomen. Het is belangrijk om in dit kader stil te staan bij hoe de verschillende drugs werken. Alcohol en koffie worden veel gebruikt, en beide drugs lenen zich daar ook goed voor.

Ecstasy wordt veel minder gebruikt, en die leent zich ook niet voor frequent gebruik, zelfs als het legaal is.

-----

Verder werd drugscriminaliteit genoemd in de comments: dat veroorzaakt een hoop slachtoffers, en die criminaliteit bestaat wel voor cocaine en ecstasy, maar niet voor alcohol.

Echter, deze criminaliteit is niet het gevolg van de drug zelf, maar van onze maatschappelijke keuze om die drug illegaal te maken.

Als we de drugs legaliseren verdwijnt die schade door drugscriminaliteit ook.

-----

Ik breid deze Mastodon thread en blog post wellicht nog uit met meer uitleg, als er nog iets langskomt dat kort genoeg uitgelegd kan worden.

Dat wil zeggen, ik breid de pot dan uit, en toot de thread dan opnieuw 😬

-----

Voor meer achtergrondinfo over hoe schadelijk alcohol is ten opzichte van cocaine, zie dit interessante interview met Tom Bart van Jellinek: [https://www.nu.nl/gezondheid/6251405/zo-schadelijk-is-een-lijntje-coke-ophef-terecht-maar-alcohol-is-ook-slecht.html](https://www.nu.nl/gezondheid/6251405/zo-schadelijk-is-een-lijntje-coke-ophef-terecht-maar-alcohol-is-ook-slecht.html){.external target="_blank"}.

Andere filmpjes van de Universiteit van Nederland over drugs staan hier: [https://www.universiteitvannederland.nl/college?search=drugs#filter](https://www.universiteitvannederland.nl/college?search=drugs#filter){.external target="_blank"}.

Andere filmpjes van de Universiteit van Nederland met collega's van de Open Universiteit staan hier: [https://www.universiteitvannederland.nl/college?school=open-universiteit#filter](https://www.universiteitvannederland.nl/college?school=open-universiteit#filter){.external target="_blank"}.

En, last en least, mijn andere filmpjes: [https://www.universiteitvannederland.nl/college/waarom-werkt-bangmakerij-niet-ontmoedigend-als-het-gaat-om-drugs](https://www.universiteitvannederland.nl/college/waarom-werkt-bangmakerij-niet-ontmoedigend-als-het-gaat-om-drugs){.external target="_blank"} en [https://www.universiteitvannederland.nl/college/moeten-we-drugs-legaliseren](https://www.universiteitvannederland.nl/college/moeten-we-drugs-legaliseren){.external target="_blank"}.
